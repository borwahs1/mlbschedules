require 'test/unit'
require 'mlbschedules'

class MLBSchedulesTest < Test::Unit::TestCase
  def test_new_game_object
    # (date, time, opponent, location, description, game_csv)
    game = MLBSchedules::Game.new("04/04/13", "03:05 PM", "Angels", "Wrigley Field", "Desc", "04/04/13,03:05 PM,Angels,Wrigley Field, Desc")

    assert_equal game.date, "04/04/13", "Date was not set correctly"
    assert_equal game.time, "03:05 PM", "Time was not set correctly"
    assert_equal game.opponent, "Los Angeles Angels", "Opponent was not set correctly"
    assert_equal game.location, "Wrigley Field", "Location was not set correctly"
    assert_equal game.description, "Desc", "Description was not set correctly"
    assert_equal game.game_csv, "04/04/13,03:05 PM,Angels,Wrigley Field, Desc", "Game CSV was not set correctly"
  end

  def test_game_object_date_time
    # (date, time, opponent, location, description, game_csv)
    game = MLBSchedules::Game.new("04/04/13", "03:05 PM", "Angels", "Wrigley Field", "Desc", "04/04/13,03:05 PM,Angels,Wrigley Field, Desc")

    datetime = DateTime.strptime("04/04/13 03:05 PM EDT",'%m/%d/%y %I:%M %p %Z')

    assert_equal game.datetime, datetime, "Date was not set correctly"
  end

  def test_game_object_to_string
    # (date, time, opponent, location, description, game_csv)
    game = MLBSchedules::Game.new("04/04/13", "03:05 PM", "Angels", "Wrigley Field", "Desc", "04/04/13,03:05 PM,Angels,Wrigley Field, Desc")

    datetime = DateTime.strptime("04/04/13 03:05 PM EDT",'%m/%d/%y %I:%M %p %Z')

    assert_equal game.to_s, "Los Angeles Angels @ Wrigley Field on #{datetime}", "Game To String failed"
  end

  def test_team_object
    team = MLBSchedules::Team.new(110, "bal", "Baltimore", "Orioles", "Oriole Park")

    assert_equal team.team_id, 110, "IDs don't match"
    assert_equal team.short_name, "bal", "Short Name was not set correctly"
    assert_equal team.city_name, "Baltimore", "City Name was not set correctly"
    assert_equal team.team_name, "Orioles", "Team Name was not set correctly"
    assert_equal team.stadium, "Oriole Park", "Stadium Name was not set correctly"
  end

  def test_team_object_full_name
    team = MLBSchedules::Team.new(110, "bal", "Baltimore", "Orioles", "Oriole Park")

    assert_equal team.full_name, "Baltimore Orioles", "Full name method incorrect"
  end

  def test_team_object_to_s
    team = MLBSchedules::Team.new(110, "bal", "Baltimore", "Orioles", "Oriole Park")

    assert_equal team.to_s, "Baltimore Orioles (bal)", "Team to string not formatted"
  end

  def test_team_object_properties_no_setter
    team = MLBSchedules::Team.new(110, "bal", "Baltimore", "Orioles", "Oriole Park")

    assert_raise NoMethodError, "Team ID property should not have a setter" do
      team.team_id = 111
    end

    assert_raise NoMethodError, "Short name property should not have a setter" do
      team.short_name = "chc"
    end

    assert_raise NoMethodError, "City Name property should not have a setter" do
      team.city_name = "Chicago"
    end

    assert_raise NoMethodError, "Team Name property should not have a setter" do
      team.team_name = "Cubs"
    end

    assert_raise NoMethodError, "Stadium property should not have a setter" do
      team.stadium = "Wrigley Field"
    end
  end

  def test_braves_short_name_team_exists
    assert MLBSchedules::TEAMS.any?{|team| team.short_name.casecmp("atl") == 0}
  end

  def test_cubs_full_name_team_exists
    assert MLBSchedules::TEAMS.any?{|team| team.full_name.casecmp("Chicago Cubs") == 0}
  end

  def test_number_of_mlb_teams
    assert_equal 30, MLBSchedules::TEAMS.length, "Expected 30 MLB Teams"
  end

  def test_create_new_schedule_object
    year = 2013
    team_id = 112
    game_type_flag = true
    home_game_flag = false

    schedule = MLBSchedules::Schedule.new(team_id, year, home_game_flag, game_type_flag)

    assert_equal team_id, schedule.team_id, "Incorrect Team ID"
    assert_equal year, schedule.year, "Year does not match"
    assert_equal game_type_flag, schedule.game_type_flag, "Game Type Flag doesn't match"
    assert_equal home_game_flag, schedule.home_game_flag, "Home Game Flag doesn't match"

    assert_not_nil schedule.url, "URL is null"
  end

  def test_regular_season_url_schedule
    year = 2013
    team_id = 112
    game_type_flag = false
    home_game_flag = true

    schedule = MLBSchedules::Schedule.new(team_id, year, home_game_flag, game_type_flag)

    url = "http://mlb.mlb.com/soa/ical/schedule.csv?home_team_id=%s&season=%s" % [team_id, year]

    assert_equal url, schedule.url, "URL is not formed for only home games"
  end

  def test_full_season_url_schedule
    year = 2013
    team_id = 112
    game_type_flag = false
    home_game_flag = false

    schedule = MLBSchedules::Schedule.new(team_id, year, home_game_flag, game_type_flag)

    url = "http://mlb.mlb.com/soa/ical/schedule.csv?team_id=%s&season=%s" % [team_id, year]

    assert_equal url, schedule.url, "URL is not formed for the full season of games"
  end

  def test_full_season_url_schedule_with_game_type
    year = 2013
    team_id = 112
    game_type_flag = true
    home_game_flag = false

    schedule = MLBSchedules::Schedule.new(team_id, year, home_game_flag, game_type_flag)

    url = "http://mlb.mlb.com/soa/ical/schedule.csv?team_id=%s&season=%s&game_type='R'" % [team_id, year]

    assert_equal url, schedule.url, "URL is not formed for only regular season games"
  end

  def test_regular_season_url_schedule
    year = 2013
    team_id = 112
    game_type_flag = true
    home_game_flag = true

    schedule = MLBSchedules::Schedule.new(team_id, year, home_game_flag, game_type_flag)

    url = "http://mlb.mlb.com/soa/ical/schedule.csv?home_team_id=%s&season=%s&game_type='R'" % [team_id, year]

    assert_equal url, schedule.url, "URL is not formed for only regular season home games"
  end
end