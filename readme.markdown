#MLB Schedules

A Ruby project to pull MLB Team schedule information from mlb.com.

##MLB Schedules as a Gem
This has not been published to RubyGems.org yet, therefore you will need to build the Gem locally after downloading this repository.

### Rake

You can use rake to build and install the gem:

    $ rake build
    
Install via rake:
    
    $ rake install
    
The default task is set to install, therefore you can run with just `rake`.

If you want to uninstall the gem, use the following:

    $ rake uninstall

### Manually

After cloning the repository, run the following from the command line:

    $ gem build mlbschedules.gemspec
    
You should see the following to confirm it built correctly:

    Successfully built RubyGem
    Name: mlbschedules
    Version: 0.0.1
    File: mlbschedules-0.0.1.gem
    
Use the new file to install the gem.

    $ gem install mlbschedules-0.0.1.gem
    
Test in IRB that it worked correctly:

    $ irb
    >> require 'mlbschedules'
    => true

###Usage

Require the gem:
    
    require 'mlbschedules'
    
Get the Team ID using `find_team_id` which expects a string for the team name:

    #examples
    team_id = MLBSchedules::find_team_id("chc")
    team_id = MLBSchedules::find_team_id("Tigers")
    team_id = MLBSchedules::find_team_id("Baltimore")

Create new instance of Schedule class with Team ID, Year, and flags:

    #params - team_id, year, home_game_flag, game_type_flag
    schedule = MLBSchedules::Schedule.new(team_id, 2013, true, true)
    
Once you have created the Schedule class, you can get the CSV text using the `request_csv` method:

    csv = schedule.request_csv
    
With the CSV text, you can parse it into an array of Game objects using `parse_csv`:

    games = schedule.parse_csv(csv)

## Team IDs

The Team IDs have been manually pulled from mlb.com. Be aware that these could potentially change.

Take a look at `teams.rb` file to see the list of teams and their id.

##License

This is MIT licensed with no added caveats. See LICENSE file for more details.

Please note that the use of this data is subjected to the terms put forth by the MLB and MLB Advanced Media.
