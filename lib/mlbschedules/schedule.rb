require 'open-uri'

module MLBSchedules

  class Schedule

    # the year to retrieve the schedule for
    attr_accessor :year

    # the team's id
    attr_accessor :team_id

    # set to true to only retrieve home games for team
    attr_accessor :home_game_flag

    # set to true to only retrieve regular season games
    attr_accessor :game_type_flag

    # the url to access the schedule
    attr_reader :url

    # Initializes a new schedule
    #
    # team_id - the team id used on MLB.com
    # year - the year to pull the schedule for
    # home_game_flag - whether to show home games only
    # game_type_flag - whether to show all games or regular season
    #
    # Examples
    #
    #   Schedule.new(112, 2013, true, false)
    #
    # Returns nothing
    def initialize(team_id, year, home_game_flag, game_type_flag)
      @team_id = team_id
      @year = year
      @home_game_flag = home_game_flag
      @game_type_flag = game_type_flag

      if home_game_flag
        path = HOME_PATH % [@team_id, @year]
      else
        path = FULL_PATH % [@team_id, @year]
      end

      if game_type_flag
        path << "&game_type='R'"
      end

      @url = BASE_URL + path
    end

    # Gets csv schedule based on team and year
    #
    # Examples
    #
    #   request_csv
    #
    # Returns response
    def request_csv

      begin
        response = open(@url) { |f| f.read }
      rescue Exception => e
        response = "ERROR: #{e.message}\nThe request for #{@url} returned an error."
      end

      response
    end

    # Parses CSV text and creates array of Game objects
    #
    # csv - the csv text to parse
    #
    # Examples
    #
    #   request_csv(csv_text)
    #
    # Returns array of Game objects
    def parse_csv(csv)
      first_row = true

      games = []

      csv_arr = csv.split("\n")

      if csv_arr.length == 0
        raise "CSV not properly formed"
      end

      column_names = get_column_names(csv_arr[0])

      csv_arr.drop(1).each do |row|
        columns = row.chomp.split(",")

        next unless columns.length > 4

        date = columns[column_names["START_DATE"]] # date : 06/13/12
        time = columns[column_names["START_TIME_ET"]] # time : 01:20 PM
        opponent = columns[column_names["SUBJECT"]].split(" at ")[0] # opponent : Astros at Cubs
        location = columns[column_names["LOCATION"]] # location : Wrigley Field
        description = columns[column_names["DESCRIPTION"]] # Local TV: FOX

        game = MLBSchedules::Game.new(date, time, opponent, location, description, row)

        games.push(game)
      end

      games
    end

    # Parses header row and maps column name to index
    #
    # header_row - the csv text for column headers
    #
    # Examples
    #
    #   get_column_names(header_row)
    #
    # Returns Hash of columns names to index in array
    def get_column_names(header_row)
      col_header_csv = header_row
      column_headers = col_header_csv.chomp.split(",")
      column_names = Hash[column_headers.map.with_index.to_a]
    end

  end

end