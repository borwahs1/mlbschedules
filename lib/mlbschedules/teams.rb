require 'json'

module MLBSchedules

  # hash storing team information with team id as the key
  TEAMS = Array.new
  TEAMS.push(Team.new(110, "bal", "Baltimore", "Orioles", "Oriole Park"))
  TEAMS.push(Team.new(111, "bos", "Boston", "Red Sox",  "Fenway Park"))
  TEAMS.push(Team.new(145, "cws", "Chicago", "White Sox", "U.S. Cellular Field"))
  TEAMS.push(Team.new(114, "cle", "Cleveland", "Indians", "Progressive Field"))
  TEAMS.push(Team.new(116, "det", "Detroit", "Tigers", "Comerica Park"))
  TEAMS.push(Team.new(117, "hou", "Houston", "Astros", "Minute Maid Park"))
  TEAMS.push(Team.new(118, "kc",  "Kansas City", "Royals", "Kauffman Stadium"))
  TEAMS.push(Team.new(108, "ana", "Los Angeles", "Angels", "Angel Stadium"))
  TEAMS.push(Team.new(142, "min", "Minnesota", "Twins", "Target Field"))
  TEAMS.push(Team.new(147, "nyy", "New York", "Yankees", "Yankee Stadium"))
  TEAMS.push(Team.new(133, "oak", "Oakland", "Athletics", "O.co Coliseum"))
  TEAMS.push(Team.new(136, "sea", "Seattle", "Mariners", "Safeco Field"))
  TEAMS.push(Team.new(139, "tb",  "Tampa Bay", "Rays", "Tropicana Field"))
  TEAMS.push(Team.new(140, "tex", "Texas", "Rangers", "Rangers Ballpark in Arlington"))
  TEAMS.push(Team.new(141, "tor", "Toronto", "Blue Jays", "Rogers Centre"))
  TEAMS.push(Team.new(109, "ari", "Arizona", "Diamondbacks", "Chase Field"))
  TEAMS.push(Team.new(144, "atl", "Atlanta", "Braves", "Turner Field"))
  TEAMS.push(Team.new(112, "chc", "Chicago", "Cubs", "Wrigley Field"))
  TEAMS.push(Team.new(113, "cin", "Cincinnati", "Reds", "Great American Ball Park"))
  TEAMS.push(Team.new(115, "col", "Colorado", "Rockies", "Coors Field"))
  TEAMS.push(Team.new(119, "la",  "Los Angeles", "Dodgers", "Dodger Stadium"))
  TEAMS.push(Team.new(146, "mia", "Miami", "Marlins", "Marlins Park"))
  TEAMS.push(Team.new(158, "mil", "Milwaukee", "Brewers", "Miller Park"))
  TEAMS.push(Team.new(121, "nym", "New York", "Mets", "Citi Field"))
  TEAMS.push(Team.new(143, "phi", "Philadelphia", "Phillies", "Citizens Bank Park"))
  TEAMS.push(Team.new(134, "pit", "Pittsburgh", "Pirates", "PNC Park"))
  TEAMS.push(Team.new(135, "sd",  "San Diego", "Padres", "PETCO Park"))
  TEAMS.push(Team.new(137, "sf",  "San Francisco", "Giants", "AT&T Park"))
  TEAMS.push(Team.new(138, "stl", "St. Louis", "Cardinals", "Busch Stadium"))
  TEAMS.push(Team.new(120, "was", "Washington", "Nationals", "Nationals Park"))

  # Returns the TEAMS hash as JSON
  #
  # Examples
  #
  #   MLBSchedules.list_team_info
  #
  # Returns json of TEAMS
  def self.list_team_info(options={})
    json = ""

    if options and options[:pretty_generate]
      json = JSON.pretty_generate(JSON.parse(TEAMS.to_json))
    else
      json = TEAMS.to_json
    end

    json
  end

  # Search TEAMS hash with property and value
  #
  # property - the property in the hash to search
  # value - the search text
  #
  # Examples
  #
  #   MLBSchedules.find_team_by_key_value(:short, "chc")
  #   MLBSchedules.find_team_by_key_value(:team, "Chicago Cubs")
  #
  # Returns index for value; nil if not found
  def self.find_team_by_key_value(property, value)
    TEAMS.find_index{|team| team.send(property).casecmp(value) == 0 }
  end

  # Gets index of team based on query
  #
  # query - team to search for
  #
  # Examples
  #
  #   MLBSchedules.find_team_index("chc")
  #   MLBSchedules.find_team_index("Chicago Cubs")
  #
  # Returns index; 0 if not found; -1 if more than one team found
  def self.find_team_index(query)
    index = self.find_team_by_key_value(:short_name, query)

    if index.nil?
      index = self.find_team_by_key_value(:full_name, query)
    end

    if index.nil?
      index = self.find_team_by_key_value(:team_name, query)
    end

    if index.nil?
      results = TEAMS.select { |team| team.city_name.casecmp(query) == 0 }

      if (results.count > 1)
        return -1
      end

      index = self.find_team_by_key_value(:city_name, query)
    end

    if index.nil?
      return 0
    end

    index
  end

  # Gets Team ID based on search query
  #
  # query - team to search for
  #
  # Examples
  #
  #   MLBSchedules.find_team_id("chc")
  #   MLBSchedules.find_team_id("Chicago Cubs")
  #
  # Returns Team ID; 0 if not found; -1 if more than one team found
  def self.find_team_id(query)
    index = self.find_team_by_key_value(:short_name, query)

    if index.nil?
      index = self.find_team_by_key_value(:full_name, query)
    end

    if index.nil?
      index = self.find_team_by_key_value(:team_name, query)
    end

    if index.nil?
      results = TEAMS.select { |team| team.city_name.casecmp(query) == 0 }

      if (results.count > 1)
        return -1
      end

      index = self.find_team_by_key_value(:city_name, query)
    end

    if index.nil?
      return 0
    end

    TEAMS[index].team_id
  end
end