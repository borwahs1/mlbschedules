module MLBSchedules

  # Version of project
  VERSION = '0.0.1'

  # Retrieve the current version
  #
  # Usage:
  #
  #   MLBSchedules.version
  #
  # Returns the current version
  def self.version
    VERSION
  end
end