module MLBSchedules

  # The base url
  BASE_URL = "http://mlb.mlb.com"

  # The path for all games on the schedule with string formats
  FULL_PATH = "/soa/ical/schedule.csv?team_id=%s&season=%s"

  # The path for home games on the schedule with string formats
  HOME_PATH = "/soa/ical/schedule.csv?home_team_id=%s&season=%s"

end