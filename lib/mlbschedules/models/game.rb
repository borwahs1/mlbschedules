module MLBSchedules

  class Game

    # date of the game
    attr_accessor :date

    # time of game; expected to be EDT
    attr_accessor :time

    # opponent name
    attr_accessor :opponent

    # stadium location for game
    attr_accessor :location

    # extra information about game
    attr_accessor :description

    # the csv text for this game
    attr_accessor :game_csv

    # Initializes a new game object
    #
    # date        - the date of the game
    # time        - the time of the game expected with EDT
    # opponent    - the opponent name
    # location    - the location of the game
    # description - extra information about the game
    # game_csv    - the csv text of the game
    #
    # Examples
    #
    #   Game.new("04/08/13","12:20:00 PM", "Chicago Cubs", "Wrigley Field")
    #
    # Returns nothing
    def initialize(date, time, opponent, location, description, game_csv)
      @date = date
      @time = time
      @location = location
      @description = description
      @game_csv = game_csv

      index = MLBSchedules.find_team_index(opponent)

      if (index > 0)
        @opponent = TEAMS[index].full_name
      else
        @opponent = opponent
      end
    end

    # Formats date and time
    #
    # Examples
    #
    #   datetime
    #
    # Returns formatted string with date and time
    def datetime
      datetime = DateTime.strptime("#{@date} #{@time} EDT",'%m/%d/%y %I:%M %p %Z')
    end

    # Returns string friendly version of Game
    #
    # Examples
    #
    #   game.to_s
    #
    # Returns formatted string with opponent, location, and date/time
    def to_s
      "#{@opponent} @ #{@location} on #{datetime}"
    end

  end

end