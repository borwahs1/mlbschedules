module MLBSchedules

  class Team

    # the MLB.com Team ID
    attr_reader :team_id

    # abbreviated team name
    attr_reader :short_name

    # team's city name
    attr_reader :city_name

    # team name
    attr_reader :team_name

    # team's stadium name
    attr_reader :stadium

    # Initializes a new Team object
    #
    # team_id      - the date of the game
    # short_name   - the time of the game expected with EDT
    # city_name    - the city name
    # team_name    - the name of the team
    # stadium      - the location of the game
    #
    # Examples
    #
    #   Team.new(112, "chc", "Chicago", "Cubs", "Wrigley Field")
    #
    # Returns nothing
    def initialize(team_id, short_name, city_name, team_name, stadium)
      @team_id = team_id
      @short_name = short_name
      @city_name = city_name
      @team_name = team_name
      @stadium = stadium
    end

    # Returns the city+team name
    #
    # Examples
    #
    #   team_obj.full_name
    #
    # Returns formatted string
    def full_name
      "#{@city_name} #{@team_name}"
    end

    # Returns string friendly version of Team object
    #
    # Examples
    #
    #   team_obj.to_s
    #
    # Returns formatted string
    def to_s
      "#{full_name} (#{@short_name})"
    end

    # Returns json of Team object
    #
    # Examples
    #
    #   team_obj.to_json
    #
    # Returns json formatted string
    def to_json(options = {})
      obj_hash = Hash.new
      obj_hash['team_id'] = @team_id
      obj_hash['short_name'] = @short_name
      obj_hash['city_name'] = @city_name
      obj_hash['team_name'] = @team_name
      obj_hash['stadium'] = @stadium

      JSON.pretty_generate(obj_hash, options)
    end

  end

end