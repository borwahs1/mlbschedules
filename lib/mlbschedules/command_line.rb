require 'optparse'

module MLBSchedules

  class CommandLine

    # Parses command line arguments and returns options
    #
    # arguments - the command line arguments
    #
    # Examples
    #
    #   MLBSchedules.get_schedule_options(arguments)
    #
    # Returns options hash based on command line arguments
    def self.get_schedule_options(arguments)
      options = {}
      args = OptionParser.new do |opts|
        opts.banner = "Usage: mlbschedules [options]"

        options[:list] = false
        opts.on("-l", "List Team Information") do |list|
          options[:list] = true
        end

        options[:team] = nil
        opts.on("-t", "--team TEAM", String, "MLB Team") do |team|
          options[:team] = team
        end

        options[:year] = Date.today.strftime("%Y")
        opts.on("-y", "--year [YEAR]", Integer, "Schedule Year") do |year|
          options[:year] = year
        end

        options[:home] = true
        opts.on("-g", "--[no-]homegames", "Only retrieve home games") do |g|
          options[:home] = g
        end

        options[:regular] = true
        opts.on("-r", "--[no-]regular", "Only retrieve regular season games") do |r|
          options[:regular] = r
        end

        opts.on("-h", "--help", "Help") do
          puts opts
          exit
        end

      end

      begin
        args.parse!(arguments)
        puts options[:list]
        if options[:list]
          puts "Team Information"
          puts MLBSchedules.list_team_info({pretty_generate:true})
          exit
        end

        mandatory_args = [:team, :year]
        missing = mandatory_args.select{ |param| options[param].nil? || options[param] == 0 }

        if not missing.empty?
          puts "Missing options: #{missing.join(', ')}"
          puts args
          exit
        end

        team_id = self.find_team_id(options[:team])

        if team_id == -1
          puts "Multiple teams found with query: #{options[:team]}"
          exit
        end

        if team_id == 0
          puts "The team could not be found: #{options[:team]}"
          exit
        end

        options[:team_id] = team_id
      rescue OptionParser::InvalidOption, OptionParser::MissingArgument => e
        puts e
        puts args
        exit
      end

      options
    end

    # Parses arguments and prints schedule to command line
    #
    # arguments - command line arguments to parse
    #
    # Examples
    #
    #   MLBSchedules.console(arguments)
    #
    # Returns nothing
    def self.run(arguments)
      options = MLBSchedules::CommandLine.get_schedule_options(arguments)

      schedule = MLBSchedules::Schedule.new(options[:team_id], options[:year], options[:home], options[:regular])
      response = schedule.request_csv

      puts schedule.parse_csv(response)
    end

  end

end