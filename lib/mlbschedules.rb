require_relative 'mlbschedules/models/game'
require_relative 'mlbschedules/models/team'

require_relative 'mlbschedules/command_line'
require_relative 'mlbschedules/globals'
require_relative 'mlbschedules/schedule'
require_relative 'mlbschedules/teams'
require_relative 'mlbschedules/version'