require File.expand_path("../lib/mlbschedules/version", __FILE__)

Gem::Specification.new do |s|
  s.name          = "mlbschedules"
  s.version       = MLBSchedules.version
  s.date          = "2013-04-27"
  s.license       = "MIT"
  s.summary       = "MLB Schedules"
  s.description   = "A gem to download the MLB Schedule from mlb.com for teams based on year."
  s.authors       = ["Rob Shaw"]
  s.email         = "rob@borwahs.com"
  s.homepage      = "https://github.com/borwahs/mlbschedules/"

  s.files         = Dir["./**/*"].reject{|file| file =~ /\.\/(doc|pkg|spec|test)/}
  s.bindir        = "bin"
  s.require_paths << "lib"
  s.executables   << "mlbschedules"
end
